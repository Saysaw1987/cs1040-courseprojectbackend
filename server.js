import express from "express";
import * as dotenv from "dotenv";
import "dotenv/config";
import cors from "cors";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import contactFormRoutes from "./routes/contactForm.js";
import loginRoutes from "./routes/login.js";
import adminRoutes from "./routes/admin.js";

// Express middleware, cors and PORT setup
import "dotenv/config";
dotenv.config();
const app = express();
app.use(
  cors({
    origin: ["http://sarah-salvatore.com"],
    methods: ["GET", "POST", "PATCH", "DELETE", "OPTIONS"],
    allowedHeaders: [
      "Content-Type",
      "Origin",
      "X-Requested-With",
      "Accept",
      "x-csrf-token",
      "Authorization",
    ],
    exposedHeaders: ["*", "Authorization"],
    credentials: true,
  })
);

const allowCrossDomain = function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://sarah-salvatore.com");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,PATCH,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
};

app.use(allowCrossDomain);

app.use(express.json());

// Cookie setup
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

const PORT = process.env.PORT || 8080;

//Routes

// contact form entries route
app.use("/entries", contactFormRoutes);
// login route
app.use("/login", loginRoutes);
// admin route
app.use("/admin", adminRoutes);

// Default route
app.get("/", (req, res) =>
  res.status(200).json(`Node and Express server running on port: ${PORT}`)
);

// Global error handler
app.use((err, req, res, next) => {
  console.error(err.stack);
  return res.status(500).json({ message: "not found" });
});

// Bind app and listen for connections on port 8080
app.listen(PORT, () =>
  console.log(`REST API server ready on http://localhost:${PORT}`)
);
