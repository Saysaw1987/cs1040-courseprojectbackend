import db from "../config/db.js";
import { validateEmail } from "./validationHelpers.js";

// Create a new form
export const createNewForm = (req, res) => {
  const name = req.body.name;
  const email = req.body.email;
  const message = req.body.message;
  const date = new Date().toISOString().slice(0, 10);

  if (!name || !message || !email) {
    return res
      .status(400)
      .json({ error: "Name, email and message are required fields." });
  }

  if (!validateEmail(email)) {
    return res
      .status(400)
      .json({ error: "Please enter a valid email address." });
  }

  if (name && message && validateEmail(email)) {
    db.query(
      "INSERT INTO contact_forms (name, email, message, dateReceived) VALUES (?, ?, ?, ?)",
      [name, email, message, date],
      (error, result) => {
        if (error) {
          return res.status(400).json({
            error: "Database query could not be completed.",
          });
        } else {
          return res.status(201).json({
            message: "New contact form entry has been posted to the database.",
          });
        }
      }
    );
  }
};

// View all contact forms
export const viewAllForms = (req, res) => {
  db.query("SELECT * FROM contact_forms", (error, result) => {
    if (error) {
      return res.status(400).json({
        error: "Database query could not be completed.",
      });
    } else {
      res.status(200).json(result);
    }
  });
};

// Add a response to form
export const logResponse = (req, res) => {
  const formId = req.params.id;
  let response = req.body.response;
  let dateResponded = new Date().toISOString().slice(0, 10);
  let query = `UPDATE contact_forms
        SET response = ?,
        dateResponded = ?
        WHERE formId = ?;`;

  db.query(query, [response, dateResponded, formId], (err, result) => {
    if (error)
      res.send(400).json({
        error: "Database query could not be completed.",
      });
    else {
      res.status(200).send(result);
    }
  });
};

// Delete a form
export const deleteContactForm = (req, res) => {
  const formId = req.params.id;
  db.query(
    "DELETE FROM contact_forms WHERE formId = ?",
    formId,
    (error, result) => {
      if (error) {
        return res.status(400).json({
          error: "Database query could not be completed.",
        });
      } else {
        return res
          .status(200)
          .json({ message: "Contact form has been deleted." });
      }
    }
  );
};
