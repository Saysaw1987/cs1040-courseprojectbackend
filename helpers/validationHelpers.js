import * as argon2 from "argon2";
import jwt from "jsonwebtoken";

// Validate email address
export const validateEmail = (email) => {
  let regex = new RegExp(
    "([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(.[!#-'*+/-9=?A-Z^-~-]+)*|[[\t -Z^-~]*])"
  );

  if (!regex.test(email)) {
    return false;
  } else {
    return true;
  }
};

// Format date
export const formatDate = () => {
  let today = new Date(date);
  let month = (today.getMonth() + 1).toString();
  let day = today.getDate().toString();
  let year = today.getFullYear();
  if (month.length < 2) {
    month = "0" + month;
  }
  if (day.length < 2) {
    day = "0" + day;
  }
  return [year, month, day].join("-");
};

// Hash password
export const hashPassword = async (password) => {
  try {
    const hash = await argon2.hash(password);
    return hash;
  } catch {
    return { error: "Error encountered when hashing password." };
  }
};

// Verify JSON token
export const verifyToken = (req, res, next) => {
  const bearerToken = req.headers.authorization.split(" ")[1];

  if (bearerToken !== "undefined") {
    jwt.verify(
      bearerToken,
      `${process.env.ACCESS_TOKEN_SECRET}`,
      (error, authData) => {
        if (error) {
          res.status(403).json({ error: "valid token not provided" });
        } else {
          next();
        }
      }
    );
  } else {
    res.status(403).json({ error: "valid token not provided" });
  }
};
