import db from "../config/db.js";
import * as argon2 from "argon2";
import jwt from "jsonwebtoken";
import * as dotenv from "dotenv";
import "dotenv/config";
dotenv.config();

// Verify password
export const verifyPassword = async (hashOnFile, enteredPassword) => {
  try {
    if (await argon2.verify(hashOnFile, enteredPassword)) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    return { error: "internal error. Cannot verify password." };
  }
};

// Generate jwt access token
export const generateAccessToken = (user) => {
  return jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "30m",
  });
};

// User login
export const userLogin = async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password) {
    return res
      .status(400)
      .json({ error: "Username and password are required." });
  }

  if (username && password) {
    db.query(
      "SELECT * FROM admin_users WHERE username = ?",
      username,
      async (error, result) => {
        if (error) {
          res.status(400).json({
            error: "Database query could not be completed.",
          });
        }

        if (result.length > 0) {
          const hashOnFile = result[0].password;
          const isValidPassword = await verifyPassword(hashOnFile, password);

          if (isValidPassword) {
            const id = result[0].userId;
            const username = result[0].username;
            const accessToken = generateAccessToken(username);

            res
              .cookie("accessToken", accessToken, {
                httpOnly: true,
                maxAge: 1800000,
                sameSite: "none",
              })
              .status(200)
              .json({ userId: id, username: username });
          } else {
            res.status(400).json({ error: `Incorrect password.` });
          }
        } else {
          res.status(404).json({ error: `User ${username} does not exist.` });
        }
      }
    );
  }
};

export const userLogout = (req, res) => {
  return res
    .clearCookie("accessToken")
    .status(200)
    .json({ message: "User logout successful." });
};
