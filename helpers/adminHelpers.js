import db from "../config/db.js";
import { hashPassword } from "./validationHelpers.js";

// Create a new user
export const createNewUser = async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const date = new Date().toISOString().slice(0, 10);

  if (!username || !password) {
    return res
      .status(400)
      .json({ error: "Username and password are required fields." });
  }

  if (username && password) {
    const hash = await hashPassword(req.body.password);

    db.query(
      "INSERT INTO admin_users (username, password, registrationDate) VALUES (?, ?, ?)",
      [username, hash, date],
      (error, result) => {
        if (error) {
          return res
            .status(400)
            .json({ error: "Database query could not be completed." });
        } else {
          return res
            .status(201)
            .json({ message: "New admin user has been created." });
        }
      }
    );
  }
};

// View all admin users
export const viewAdminUsers = (req, res) => {
  db.query("SELECT * FROM admin_users", (error, result) => {
    if (error) {
      return res.status(400).json({
        error: "Database query could not be completed.",
      });
    } else {
      res.status(200).json(result);
    }
  });
};

// Delete an admin user
export const deleteAdminUser = (req, res) => {
  const userId = req.params.id;
  db.query(
    "DELETE FROM admin_users WHERE userId = ?",
    userId,
    (error, result) => {
      if (error) {
        return res
          .status(400)
          .json({ error: "Database query could not be completed." });
      } else {
        return res
          .status(200)
          .json({ message: "Admin user has been deleted." });
      }
    }
  );
};

export const editAdminUser = async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password) {
    return res.status(400).json({
      error: "Existing username and new password are required fields.",
    });
  }
  if (username && password) {
    const hash = await hashPassword(req.body.password);

    let query = `UPDATE admin_users
      SET password = ?
      where username = ?;`;

    db.query(query, [hash, username], (error, result) => {
      if (error) {
        res.send(400).json({ error: "Database query could not be completed." });
      } else {
        res.status(200).json({ message: "User password has been updated." });
      }
    });
  }
};
