import jwt from "jsonwebtoken";
import * as dotenv from "dotenv";
import "dotenv/config";
dotenv.config();

export const validateToken = (req, res, next) => {
  const accessToken = req.cookies.accessToken;

  if (!accessToken) {
    res.status(400).json({ message: "No token provided." });
  } else {
    jwt.verify(
      accessToken,
      process.env.ACCESS_TOKEN_SECRET,
      (error, decoded) => {
        if (error) {
          res
            .status(403)
            .json({ auth: false, error: "Valid token not provided." });
        } else {
          req.userId = decoded.userId;
          next();
        }
      }
    );
  }
};
