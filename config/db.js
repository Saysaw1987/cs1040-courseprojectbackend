import mysql from "mysql2";
import * as dotenv from "dotenv";
import "dotenv/config";
dotenv.config();

// Database connection
// const db = mysql.createPool({
//   user: process.env.DB_USER,
//   host: process.env.DB_HOST,
//   password: process.env.DB_PASSWORD,
//   database: process.env.DB_DATABASE,
//   connectionLimit: 10,
// });

const configuration = {
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
};

if (process.env.DATABASE_SOCKET) {
  configuration.socketPath = process.env.DATABASE_SOCKET;
} else {
  configuration.host = process.env.DATABASE_HOST;
}

const db = mysql.createConnection(configuration);

export default db;
