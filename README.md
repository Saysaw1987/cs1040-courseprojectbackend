# The Full Stack Dev Portfolio of Sarah Salvatore

Hello! Thank you for your interest in my Full Stack Dev Portfolio. Please continue reading for more details:

## Definitions

- **API**- Application Programming Interface
- **REST**- REpresentational State Transfer
- **CRUD**- CRUD is an acronym for Create Read Update Delete, a set of rules to follow when building an API.
- **JSON**- Javascript Object Notation. It is a common format for sending and requesting data through a REST API.
- **HTTP**- Hypertext Transfer Protocol. It gives users a way to interact with web resources by transmitting hypertext messages between clients and servers.

## How to Run Locally

- **Run 'npm i' Command**: Run npm install to install all dependencies.

- **Run 'npm run start' Command**: Run npm run start to start the server.

## How to Run Containers

- **Run docker-compose up**: Run docker-compose up to build and start the api and db containers.

- **Run docker-compose down**: Run docker-compose down to stop the containers. Note: All data will be lost.

## Deployment

- The API/Server has been deployed using Google Cloud Run
- The Database has been created using Google SQL

## Additional Info

- **Start**: node server.js

- **Dev Start**: nodemon server.js

- **Container Start**: docker-compose up

- **Container Exit**: docker-compose down

- **Local Port**: This server runs locally at: **http://localhost:8080**

- **Socket Path**: A socket path is enabled using GCP

## HTTP Methods

- **GET**- gets data from the backend.
- **POST**- updates data.
- **PATCH**- updates data.
- **DELETE** - deletes data.

## Routes

The following routes are available:

- **/login**
- **/entries**
- **/admin**

## HTTP Codes

The following HTTP codes have been used within this project.

- **200** - Okay
- **201** - Accepted
- **400** - Bad Request
- **401** - Unauthorized
- **403** - Forbidden
- **404** - Not Found
- **500** - Internal Server Error

## Package Setup

This project contains:

- Main file - server.js
- Package.json file and a package-lock.json file.
- Routes can be found via the routes folder.
- Helper functions can be found via the helpers folder.
- Environment variables can be found via the .env file.
- Database configuration can be found in the config folder.

## Contributions

Public repositories. Pull requests are welcomed.

## License

ISC License

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
Source: http://opensource.org/licenses/ISC
