import express from "express";
import {
  createNewForm,
  deleteContactForm,
  logResponse,
  viewAllForms,
} from "../helpers/contactFormHelpers.js";
// import { validateToken } from "../middleware/accessTokenAuth.js";

const router = express.Router();

// Create a new contact form entry
router.post("/", createNewForm);

// Get all contact forms
router.get("/", viewAllForms);

// Update a contact form
router.put("/", logResponse);

// Delete a contact form
router.delete("/:id", deleteContactForm);

export default router;
