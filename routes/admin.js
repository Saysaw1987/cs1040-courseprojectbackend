import express from "express";
import {
  createNewUser,
  deleteAdminUser,
  editAdminUser,
  viewAdminUsers,
} from "../helpers/adminHelpers.js";
// import { validateToken } from "../middleware/accessTokenAuth.js";

const router = express.Router();

// Create a new admin
router.post("/", createNewUser);

// Get all admin users
router.get("/", viewAdminUsers);

// Update an admin user
router.patch("/", editAdminUser);

// Delete an admin user
router.delete("/:id", deleteAdminUser);

export default router;
