import express from "express";
import { userLogin, userLogout } from "../helpers/loginHelpers.js";

const router = express.Router();

// Login
router.post("/", userLogin);

// Logout
router.get("/", userLogout);

export default router;
